# HeatmapCalendar

A Heatmap calendar project, built by composition of two distinct components:
the Calendar and the Month. Both are served by a 'heat data' service which
provides an abstract summary of activity, as well as indicates at which point
in time the activities start and at which point it ends.

The Calendar component introspects the activity summary to decide which Months
to render, and the Month component renders a calendar of that given month, with
the days color-coded by how much activity it had during that day.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.6.

# Requirements

To run the project you need the Angular framework, follow instructions on its
guide to get the set-up necessary: https://angular.io/guide/quickstart

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
