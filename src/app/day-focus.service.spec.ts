import { TestBed, inject } from '@angular/core/testing';

import { DayFocusService } from './day-focus.service';

describe('DayFocusService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DayFocusService]
    });
  });

  it('should be created', inject([DayFocusService], (service: DayFocusService) => {
    expect(service).toBeTruthy();
  }));
});
