import { Component, OnInit } from '@angular/core';
import { HeatDataService } from '../heat-data.service';
import { ActivitySummary } from '../activitySummary';

/**
 * The Calendar component introspects the activity summary to decide which
 * Months to render.
 */
@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit {
  private activitySummary: ActivitySummary;

  constructor(private heatDataService: HeatDataService) { }

  public ngOnInit() {
    this.subscribeToActivitySummary();
  }

  private subscribeToActivitySummary(): void {
    this.heatDataService.getActivitySummary()
      .subscribe(activitySummary => this.activitySummary = activitySummary);
  }

  /**
   * Finds all months in the activity summary's interval
   */
  private getMonths(): Date[] {
    const intervalStart = this.activitySummary.getIntervalStart();
    const intervalEnd = this.activitySummary.getIntervalEnd();

    let year = intervalStart.getFullYear();
    let month = intervalStart.getMonth();
    const yearEnd = intervalEnd.getFullYear();
    const monthEnd = intervalEnd.getMonth();

    const dates = [];
    while ((year < yearEnd) || (year === yearEnd && month <= monthEnd)) {
      dates.push(new Date(year, month));
      month++;
      if (month > 12) {
        month = 1;
        year++;
      }
    }

    return dates;
  }
}
