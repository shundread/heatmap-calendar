import { Component, OnInit } from '@angular/core';

import { HeatDataService } from '../heat-data.service';
import { DayFocusService } from '../day-focus.service';

import { ActivitySummary } from '../activitySummary';

/**
 * The DaySummary component can display the list of events of an activity
 * summary, provided it is fed with data from a DayFocusService and a
 * HeatDataService
 */
@Component({
  selector: 'app-day-summary',
  templateUrl: './day-summary.component.html',
  styleUrls: ['./day-summary.component.css']
})
export class DaySummaryComponent implements OnInit {
  activitySummary: ActivitySummary;

  constructor(
    public dayFocusService: DayFocusService,
    private heatDataService: HeatDataService
  ) { }

  public ngOnInit() {
    this.subscribeToActivitySummary();
  }

  private subscribeToActivitySummary(): void {
    this.heatDataService.getActivitySummary()
      .subscribe(activitySummary => this.activitySummary = activitySummary);
  }

  private getActivitySummary(): string[] {
    if (!this.dayFocusService.date) {
      return [];
    }
    return this.activitySummary.getDateActivities(this.dayFocusService.date);
  }

  ////////////////////////
  // Date introspection //
  ////////////////////////

  private getYear(): number {
    return this.dayFocusService.date.getFullYear();
  }

  private getMonthName(): string {
    return this.dayFocusService.date.toLocaleString('en', { month: 'long' });
  }

  private getDay(): number {
    return this.dayFocusService.date.getDate();
  }
}
