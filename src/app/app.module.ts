import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { MonthComponent } from './month/month.component';
import { HeatDataService } from './heat-data.service';
import { CalendarComponent } from './calendar/calendar.component';
import { DaySummaryComponent } from './day-summary/day-summary.component';
import { DayFocusService } from './day-focus.service';


@NgModule({
  declarations: [
    AppComponent,
    MonthComponent,
    CalendarComponent,
    DaySummaryComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [HeatDataService, DayFocusService],
  bootstrap: [AppComponent]
})
export class AppModule { }
