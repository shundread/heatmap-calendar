import { Injectable } from '@angular/core';

@Injectable()
export class DayFocusService {
  date: Date|null = null;

  constructor() { }

  /**
   * Sets the focused day for the service users
   */
  setFocusedDay (date: Date) {
    this.date = date;
  }
}
