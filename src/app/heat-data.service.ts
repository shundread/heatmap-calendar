import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { ACTIVITY_SUMMARY } from './mock-activity';
import { ActivitySummary } from './activitySummary';

@Injectable()
export class HeatDataService {

  constructor() { }

  getActivitySummary (): Observable<ActivitySummary> {
    return of(ACTIVITY_SUMMARY);
  }

}
