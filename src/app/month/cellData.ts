export interface ICellData {
  date?: Date;
  day: number;
  row: number;
  col: number;
  displayAs: string;
}
