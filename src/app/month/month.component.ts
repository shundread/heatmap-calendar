import { Component, OnInit, Input } from '@angular/core';

import { HeatDataService } from '../heat-data.service';
import { DayFocusService } from '../day-focus.service';

import { ActivitySummary, EntryList } from '../activitySummary';

import { ICellData } from './cellData';

const Levels = 4;
const Columns = 7;

/**
 * The Month component renders a calendar of that given month (in a given year),
 * with the days color-coded by how much activity it had during that day.
 */
@Component({
  selector: 'app-month',
  templateUrl: './month.component.html',
  styleUrls: ['./month.component.css']
})
export class MonthComponent implements OnInit {
  @Input() startingDate: Date;
  private activitySummary: ActivitySummary;

  weekdays: Array<string> = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];

  constructor(
    public dayFocusService: DayFocusService,
    private heatDataService: HeatDataService
  ) { }

  public ngOnInit() {
    this.subscribeToActivitySummary();
  }

  private subscribeToActivitySummary (): void {
    this.heatDataService.getActivitySummary()
      .subscribe(activitySummary => this.activitySummary = activitySummary);
  }

  /**
   * Returns an array of all the grid cells, including padding cells which are
   * needed to prevent the browser renderer from taking too many liberties on
   * how to arrange the grid
   */
  private getCells(): ICellData[] {
    // Grab the first day of the month so we can start the calendar at the right
    // column
    const startingDay = this.getFirstDay();
    const nCells = startingDay + this.getMonthDays();
    const cells = [];
    for (let i = 0; i < nCells; i++) {
      const day = i - startingDay;
      const row = 2 + Math.floor(i / Columns);
      const col = (1 + i) % Columns;

      // Pad the time before the first day of the with empty cells so the grid
      // alignment doesn't break
      if (day < 0) {
        cells.push({ day, row, col, displayAs: 'hide' });
      } else {
        const date = new Date(this.getYear(), this.getMonth(), day + 1);
        const details = this.activitySummary.getDateActivities(date);

        // TODO: assemble cell tooltip information using details of activities,
        //       which could be, for example, commit messages or additions and
        //       removals or even just 'number of commits'

        const levelIndex = Math.ceil(Levels * details.length /
            this.activitySummary.activityQuantityOnBusiestDay());

        // TODO: This mixing of the display information within the component
        //       is bogus and should be addressed later

        const displayAs = `day level-${levelIndex}`;
        cells.push({ date, day, row, col, displayAs });
      }
    }
    return cells;
  }

  //
  // Date introspection
  //

  private getMonthName(): string {
    return this.startingDate.toLocaleString('en', { month: 'long' });
  }

  private getYear(): number {
    return this.startingDate.getFullYear();
  }

  private getMonth(): number {
    return this.startingDate.getMonth();
  }

  private getFirstDay(): number {
    // NOTE: For now not assuming that we were given the first day of the month
    const year = this.getYear();
    const month = this.getMonth();
    const firstDay = new Date(year, month, 1);
    return firstDay.getDay();
  }

  private getMonthDays(): number {
    const year = this.getYear();
    const month = this.getMonth();
    const nextMonthDate = new Date(year, month + 1, 0);
    return nextMonthDate.getDate();
  }
}
