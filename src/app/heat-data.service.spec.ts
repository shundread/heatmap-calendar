import { TestBed, inject } from '@angular/core/testing';

import { HeatDataService } from './heat-data.service';

describe('HeatDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HeatDataService]
    });
  });

  it('should be created', inject([HeatDataService], (service: HeatDataService) => {
    expect(service).toBeTruthy();
  }));
});
