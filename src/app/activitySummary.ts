export type EntryList = string[];

/**
 * This structure takes in activity entries and provides the user with functions
 * to determine the start-end interval (with the granularity of a day) for the
 * set of activities, as well as perform lookup for activities registered on any
 * given day (including days outside the interval)
 */
export class ActivitySummary {
  activities: { [date: string]: EntryList };
  highestActivityQuantity: number;
  intervalStart: string;
  intervalEnd: string;

  constructor () {
    this.activities = {};
    this.highestActivityQuantity = 0;
  }

  /**
   * Adds an activity to the summary
   */
  public addActivity(date: Date, detail: string = '') {
    const dateString = this._toDateString(date);
    if (!this.activities[dateString]) {
      this.activities[dateString] = [];
    }
    const length = this.activities[dateString].push(detail);
    this.highestActivityQuantity =
        Math.max(length, this.activityQuantityOnBusiestDay());

    this._updateInterval(dateString);
  }

  /**
   * Returns the count of activities on the busiest day for this summary
   */
  public activityQuantityOnBusiestDay(): number {
    return this.highestActivityQuantity;
  }

  /**
   * Returns a list of activities that were registered for a particular day
   */
  public getDateActivities(date: Date): EntryList {
    const dateString = this._toDateString(date);
    return (this.activities[dateString] || []).concat();
  }

  public getIntervalStart(): Date {
    return new Date(this.intervalStart);
  }

  public getIntervalEnd(): Date {
    return new Date(this.intervalEnd);
  }

  /**
   * Converts a Date object to the strings that are used as keys to the summary
   */
  private _toDateString(date: Date): string {
    const year = date.getFullYear();
    const month = date.getMonth();
    const day = date.getDate();
    return `${year}-${String(month + 1).padStart(2, '0')}-${
        String(day).padStart(2, '0')}`;
  }

  /**
   * Ensures intervalStart - intervalEnd are at the extremities of the summary
   */
  private _updateInterval(dateString: string): void {
    if (!this.intervalStart || this.intervalStart > dateString) {
      this.intervalStart = dateString;
    }
    if (!this.intervalEnd || this.intervalEnd < dateString) {
      this.intervalEnd = dateString;
    }
  }
}
